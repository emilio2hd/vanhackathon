require 'test_helper'

class UserFlowsTest < ActionDispatch::IntegrationTest

  test "registration" do
    https!
    get "/auth/login"
    assert_response :success

    post_via_redirect "/login", username: users(:one).email, password: users(:one).password
    assert_equal 'Signed in successfully.', flash[:notice]
  end

end
