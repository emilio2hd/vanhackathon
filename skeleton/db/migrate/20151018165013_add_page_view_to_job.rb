class AddPageViewToJob < ActiveRecord::Migration
  def change
    add_column :jobs, :page_view, :integer
  end
end
