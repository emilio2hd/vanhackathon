class AddCategoryToJob < ActiveRecord::Migration
  def change
    add_reference :jobs, :skill_category, index: true, foreign_key: true
  end
end
