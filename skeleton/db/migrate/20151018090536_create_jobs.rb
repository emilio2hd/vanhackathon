class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.text :description
      t.string :coworkers
      t.string :skills
      t.string :annual_salary_range
      t.references :company, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
