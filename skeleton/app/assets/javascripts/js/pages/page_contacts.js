var ContactPage = function () {

    return {
        
    	//Basic Map
        initMap: function () {
			var map;
			$(document).ready(function(){
			  map = new GMaps({
				div: '#map',
				scrollwheel: false,				
				lat: 49.281977,
				lng: -123.108162
			  });
			  
			  var marker = map.addMarker({
				lat: 49.281977,
				lng: -123.108162,
	            title: 'Company, Inc.'
		       });
			});
        },

        //Panorama Map
        initPanorama: function () {
		    var panorama;
		    $(document).ready(function(){
		      panorama = GMaps.createPanorama({
		        el: '#panorama',
		        lat : 49.281977,
		        lng : -123.108162
		      });
		    });
		}        

    };
}();