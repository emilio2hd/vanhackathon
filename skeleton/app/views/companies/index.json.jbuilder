json.array!(@companies) do |company|
  json.extract! company, :id, :name, :website, :location, :markets, :high_concept_pitch, :why_us, :product
  json.url company_url(company, format: :json)
end
