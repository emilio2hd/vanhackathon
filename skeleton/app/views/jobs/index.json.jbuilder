json.array!(@jobs) do |job|
  json.extract! job, :id, :title, :description, :coworkers, :skills, :annual_salary_range, :company_id
  json.url job_url(job, format: :json)
end
