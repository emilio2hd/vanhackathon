class HomeController < ApplicationController

  def index
  end

  def blog
  end

  def blog_post
    restricted_area
  end

  def cart
  end

  def about
  end

  def contact
  end

  def invoice
  end

  def premium
  end

  def jobs
    @companies = Company.all
  end

end
