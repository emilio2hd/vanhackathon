class AccountController < ApplicationController

  before_action :authenticate_user!
  before_action :set_account, only: [:index, :edit, :update]

  def index
    render "profile"
  end

  # GET /account/1/edit
  def edit
    render "profile"
  end

  # PATCH/PUT /account/1
  # PATCH/PUT /account/1.json
  def update

    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Account was successfully updated.' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end


  private
  # Use callbacks to share common setup or constraints between actions.
  def set_account
    @account = User.find(current_user.id)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def account_params
    params.require(:user).permit(:first_name, :last_name, :address, :country, :city, :description, :phone, :linkedin, :skype, :github)
  end

end