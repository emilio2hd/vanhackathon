class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :skills
  has_many :projects

  def has_restrict_access?
    return false
  end

  def can_register_company?
    return false
  end

  def premium?
    !stripeToken.nil?
  end

  def rank_position
    "19"
  end

  def average_score
    "78/100"
  end

end
